package core.skywars.lands;

import core.skywars.lands.api.map.MapLoader;
import core.skywars.lands.commands.CmdSetChest;
import core.skywars.lands.commands.CmdSetLocation;
import core.skywars.lands.commands.CmdWorld;
import core.skywars.lands.game.Game;
import core.skywars.lands.game.GameState;
import core.skywars.lands.listeners.*;
import core.skywars.lands.listeners.items.ListenerLeave;
import core.skywars.lands.listeners.items.ListenerMapVote;
import core.skywars.lands.listeners.items.ListenerSetChest;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Core extends JavaPlugin {

    private static Core instance;
    private static Game game;
    private MapLoader mapLoader;

    public void onEnable() {
        instance = this;
        game = new Game();

        this.registerListeners();
        this.registerCommands();

        game.getMySQL().createRoom();

        this.mapLoader = new MapLoader("skywars").setupMaps();

        Bukkit.setWhitelist(false);

        getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");

        Core.getGame().getMySQL().setRoomState(GameState.WAITING.toString());
        Core.getGame().getMySQL().setRoomPlayers(0);
    }

    public void onDisable() {
        mapLoader.removeMaps();
        Bukkit.getServer().setWhitelist(false);
    }

    private void registerListeners() {
        PluginManager pluginManager = Bukkit.getPluginManager();
        pluginManager.registerEvents(new ListenerSetChest(), this);
        pluginManager.registerEvents(new ListenerLeave(), this);
        pluginManager.registerEvents(new ListenerMapVote(), this);
        pluginManager.registerEvents(new ListenerMainProtection(), this);
        pluginManager.registerEvents(new ListenerPlayerChat(), this);
        pluginManager.registerEvents(new ListenerPlayerDamage(), this);
        pluginManager.registerEvents(new ListenerPlayerJoin(), this);
        pluginManager.registerEvents(new ListenerPlayerQuit(), this);
    }

    private void registerCommands() {
        getCommand("setchest").setExecutor(new CmdSetChest());
        getCommand("setlocation").setExecutor(new CmdSetLocation());
        getCommand("world").setExecutor(new CmdWorld());
    }

    public static Game getGame() {
        return game;
    }

    public static Core getInstance() {
        return instance;
    }

}