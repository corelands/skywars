package core.skywars.lands.managers.scoreboard;

import core.skywars.lands.Core;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.Scoreboard;

public class ScoreboardAnimator extends BukkitRunnable {

    int time = 0;

    @Override
    public void run() {
        time++;
        for (Player all : Bukkit.getOnlinePlayers()) {
            Scoreboard board = all.getScoreboard();

            board.getTeam("left").setSuffix("§f" + Core.getGame().getGameConfig().getPlayersLeft());

            board.getTeam("map").setSuffix("§f" + Core.getGame().getGameConfig().getActiveMap());

            board.getTeam("time").setSuffix("§f" + getTime());
        }
    }

    public String getTime() {
        int minutes = 0;
        int seconds = 0;

        minutes = time/60;
        seconds = time%60;

        String timeString = "";

        if(minutes < 10) {
            timeString+="0";
        }
        timeString+=minutes;

        timeString+=":";

        if(seconds < 10) {
            timeString+="0";
        }
        timeString+=seconds;

        return timeString;
    }

}
