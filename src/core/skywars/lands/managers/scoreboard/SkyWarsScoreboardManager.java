package core.skywars.lands.managers.scoreboard;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;

public class SkyWarsScoreboardManager {

    private Scoreboard board;
    private Objective obj;
    private Player p;

    public SkyWarsScoreboardManager(Player p) {
        this.board = Bukkit.getScoreboardManager().getNewScoreboard();
        this.obj = board.registerNewObjective("SkyWars", "CoreLands");
        this.obj.setDisplaySlot(DisplaySlot.SIDEBAR);
        this.obj.setDisplayName("§bSkyWars");
        this.p = p;
    }

    public void setScoreboard() {

        obj.getScore("§a ").setScore(5);

        Team left = board.registerNewTeam("left");
        left.addEntry("§ePlayers Left: ");
        left.setSuffix("§f0");
        obj.getScore("§ePlayers Left: ").setScore(4);

        obj.getScore("§b ").setScore(3);

        Team map = board.registerNewTeam("map");
        map.addEntry("§eMap: ");
        map.setSuffix("§fUnknown");
        obj.getScore("§eMap: ").setScore(2);

        obj.getScore("§c ").setScore(1);

        Team time = board.registerNewTeam("time");
        time.addEntry("§eTime: ");
        time.setSuffix("§f00:00");
        obj.getScore("§eTime: ").setScore(0);

        this.p.setScoreboard(board);


    }
}