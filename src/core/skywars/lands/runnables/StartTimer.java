package core.skywars.lands.runnables;

import core.skywars.lands.Core;
import core.skywars.lands.data.PlayerData;
import core.skywars.lands.game.GameState;
import core.skywars.lands.game.chests.ChestTypes;
import core.skywars.lands.managers.scoreboard.ScoreboardAnimator;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Chest;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.Random;

public class StartTimer extends BukkitRunnable {

    private int time, startTime;

    public StartTimer() {
        time = Core.getGame().getGameConfig().getStartTime();
        startTime = time-1;
    }

    public void run() {
        time--;

        for(Player all : Bukkit.getOnlinePlayers()) {
            all.setLevel(time);
            all.setExp((float) ((time*1.0)/(startTime*1.0)));

            if((time == 60 || time == 30 || time == 15 || time == 10 || time <= 5) && time > 0) {
                all.playSound(all.getLocation(), Sound.UI_BUTTON_CLICK, 1f, 1f);
            }
        }
        if((time == 60 || time == 30 || time == 15 || time == 10 || time <= 5) && time > 0) {
            Bukkit.broadcastMessage(Core.getGame().getGameConfig().getPrefix() + " §7Starting in §e" + time + " §7seconds.");
        }

        if(time == 0) {
            if(Bukkit.getOnlinePlayers().size() >= Core.getGame().getGameConfig().getMinPlayers()) {
                start();
            } else {
                stop();
            }
            this.cancel();
        }

    }

    private void start() {
        String winnerMap = Core.getGame().getGameConfig().getMaps().get(0);

        int maxVotes = 0;

        for(String map : Core.getGame().getGameConfig().getMaps()) {
            if(Core.getGame().getGameConfig().getMapVotes(map) > maxVotes) {
                maxVotes = Core.getGame().getGameConfig().getMapVotes(map);
                winnerMap = map;
            }
        }

        Bukkit.broadcastMessage(Core.getGame().getGameConfig().getPrefix() + " §7Map §e" + winnerMap);
        Core.getGame().getGameConfig().setActiveMap(winnerMap);

        this.setupPlayers();

        Core.getGame().setGameState(GameState.IN_GAME);
        Core.getGame().getMySQL().setRoomState(GameState.IN_GAME.toString());
        new ScoreboardAnimator().runTaskTimer(Core.getInstance(), 0, 20);
    }

    private void setupPlayers() {
        int id = 0;
        for(Player player : Bukkit.getOnlinePlayers()) {
            PlayerData playerData = Core.getGame().getPlayerData(player);
            playerData.setIsland(getIsland(id));
            playerData.teleportIsland();
            player.getScoreboard().getTeam("map").setSuffix("§f" + Core.getGame().getGameConfig().getActiveMap());
            player.getInventory().clear();
            player.getInventory().setArmorContents(null);
            player.setItemOnCursor(null);
            id++;
        }

        this.fillChests();

        new BukkitRunnable() {
            int spawnTime = 11;
            @Override
            public void run() {
                spawnTime--;

                for(Player all : Bukkit.getOnlinePlayers()) {
                    all.setLevel(spawnTime);
                    all.setExp((float) ((spawnTime*1.0)/(11*1.0)));

                    if((spawnTime == 10 || spawnTime <= 5) && spawnTime > 0) {
                        all.playSound(all.getLocation(), Sound.UI_BUTTON_CLICK, 1f, 1f);
                    }
                }
                if((spawnTime == 10 || spawnTime <= 5) && spawnTime > 0) {
                    Bukkit.broadcastMessage(Core.getGame().getGameConfig().getPrefix() + " §7Opening cage in §e" + spawnTime + " §7seconds.");
                }
                if(spawnTime == 0) {
                    Bukkit.broadcastMessage(Core.getGame().getGameConfig().getPrefix() + " §7Good luck =)");
                    for(Player player : Bukkit.getOnlinePlayers()) {
                        PlayerData playerData = Core.getGame().getPlayerData(player);
                        double adjust = -1.5;
                        for(int i = 0; i < 5; i++) {
                            player.getWorld().getBlockAt(playerData.getIsland().clone().add(0, adjust, 0)).setType(Material.AIR);
                            player.getWorld().getBlockAt(playerData.getIsland().clone().add(0, adjust, 1)).setType(Material.AIR);
                            player.getWorld().getBlockAt(playerData.getIsland().clone().add(0, adjust, -1)).setType(Material.AIR);
                            player.getWorld().getBlockAt(playerData.getIsland().clone().add(1, adjust, 0)).setType(Material.AIR);
                            player.getWorld().getBlockAt(playerData.getIsland().clone().add(-1, adjust, 0)).setType(Material.AIR);
                            player.getWorld().getBlockAt(playerData.getIsland().clone().add(1, adjust, 1)).setType(Material.AIR);
                            player.getWorld().getBlockAt(playerData.getIsland().clone().add(1, adjust, -1)).setType(Material.AIR);
                            player.getWorld().getBlockAt(playerData.getIsland().clone().add(-1, adjust, -1)).setType(Material.AIR);
                            player.getWorld().getBlockAt(playerData.getIsland().clone().add(-1, adjust, 1)).setType(Material.AIR);
                            adjust+=1;
                        }
                        playerData.spawn();
                    }

                    Core.getGame().getGameConfig().setPlayersLeft(Bukkit.getOnlinePlayers().size());

                    new ScoreboardAnimator().runTaskTimer(Core.getInstance(), 20, 20);
                    this.cancel();
                }
            }
        }.runTaskTimer(Core.getInstance(), 0, 20);
    }

    private void fillChests() {

        ChestTypes chestTypes = new ChestTypes();
        ArrayList<ItemStack> iChests = chestTypes.getIslandChests(), smChests = chestTypes.getSemiMidChests(), mChests = chestTypes.getMidChests();

        for(String islandChests : Core.getInstance().getConfig().getConfigurationSection(Core.getGame().getGameConfig().getActiveMap() + ".chest.normal").getKeys(false)) {
            Location chestLocation = new Location(
                    Bukkit.getWorld(Core.getInstance().getConfig().getString(Core.getGame().getGameConfig().getActiveMap() + ".chest.normal." + islandChests + ".world")),
                    Core.getInstance().getConfig().getInt(Core.getGame().getGameConfig().getActiveMap() + ".chest.normal." + islandChests + ".x"),
                    Core.getInstance().getConfig().getInt(Core.getGame().getGameConfig().getActiveMap() + ".chest.normal." + islandChests + ".y"),
                    Core.getInstance().getConfig().getInt(Core.getGame().getGameConfig().getActiveMap() + ".chest.normal." + islandChests + ".z")
            );

            if(chestLocation.getBlock().getType() == Material.CHEST) {
                Chest chest = (Chest) chestLocation.getBlock().getState();

                Inventory inventory = chest.getBlockInventory();

                ArrayList<ItemStack> noRepeat = new ArrayList<ItemStack>();

                for (int i = 0; i < 6; i++) {
                    ItemStack randomItemStack = iChests.get(new Random().nextInt(iChests.size()));
                    while(noRepeat.contains(randomItemStack)) {
                        randomItemStack = iChests.get(new Random().nextInt(iChests.size()));
                    }
                    noRepeat.add(randomItemStack);
                    inventory.setItem(new Random().nextInt(inventory.getSize()), randomItemStack);
                }
            }

        }

        for(String semiMidChests : Core.getInstance().getConfig().getConfigurationSection(Core.getGame().getGameConfig().getActiveMap() + ".chest.semimid").getKeys(false)) {
            Location chestLocation = new Location(
                    Bukkit.getWorld(Core.getInstance().getConfig().getString(Core.getGame().getGameConfig().getActiveMap() + ".chest.semimid." + semiMidChests + ".world")),
                    Core.getInstance().getConfig().getInt(Core.getGame().getGameConfig().getActiveMap() + ".chest.semimid." + semiMidChests + ".x"),
                    Core.getInstance().getConfig().getInt(Core.getGame().getGameConfig().getActiveMap() + ".chest.semimid." + semiMidChests + ".y"),
                    Core.getInstance().getConfig().getInt(Core.getGame().getGameConfig().getActiveMap() + ".chest.semimid." + semiMidChests + ".z")
            );

            if(chestLocation.getBlock().getType() == Material.CHEST) {
                Chest chest = (Chest) chestLocation.getBlock().getState();

                Inventory inventory = chest.getBlockInventory();

                ArrayList<ItemStack> noRepeat = new ArrayList<ItemStack>();

                for (int i = 0; i < 6; i++) {
                    ItemStack randomItemStack = smChests.get(new Random().nextInt(smChests.size()));
                    while(noRepeat.contains(randomItemStack)) {
                        randomItemStack = smChests.get(new Random().nextInt(smChests.size()));
                    }
                    noRepeat.add(randomItemStack);
                    inventory.setItem(new Random().nextInt(inventory.getSize()), randomItemStack);
                }
            }

        }

        for(String midChests : Core.getInstance().getConfig().getConfigurationSection(Core.getGame().getGameConfig().getActiveMap() + ".chest.mid").getKeys(false)) {
            Location chestLocation = new Location(
                    Bukkit.getWorld(Core.getInstance().getConfig().getString(Core.getGame().getGameConfig().getActiveMap() + ".chest.mid." + midChests + ".world")),
                    Core.getInstance().getConfig().getInt(Core.getGame().getGameConfig().getActiveMap() + ".chest.mid." + midChests + ".x"),
                    Core.getInstance().getConfig().getInt(Core.getGame().getGameConfig().getActiveMap() + ".chest.mid." + midChests + ".y"),
                    Core.getInstance().getConfig().getInt(Core.getGame().getGameConfig().getActiveMap() + ".chest.mid." + midChests + ".z")
            );

            if(chestLocation.getBlock().getType() == Material.CHEST) {
                Chest chest = (Chest) chestLocation.getBlock().getState();

                Inventory inventory = chest.getBlockInventory();

                ArrayList<ItemStack> noRepeat = new ArrayList<ItemStack>();

                for (int i = 0; i < 6; i++) {
                    ItemStack randomItemStack = mChests.get(new Random().nextInt(mChests.size()));
                    while(noRepeat.contains(randomItemStack)) {
                        randomItemStack = mChests.get(new Random().nextInt(mChests.size()));
                    }
                    noRepeat.add(randomItemStack);
                    inventory.setItem(new Random().nextInt(inventory.getSize()), randomItemStack);
                }
            }

        }
    }

    private Location getIsland(int id) {
        FileConfiguration config = Core.getInstance().getConfig();

        return new Location(Bukkit.getWorld(config.getString(Core.getGame().getGameConfig().getActiveMap() + ".spawnLocation." + id + ".world")),
                config.getDouble(Core.getGame().getGameConfig().getActiveMap() + ".spawnLocation." + id + ".x"),
                config.getDouble(Core.getGame().getGameConfig().getActiveMap() + ".spawnLocation." + id + ".y"),
                config.getDouble(Core.getGame().getGameConfig().getActiveMap() + ".spawnLocation." + id + ".z"),
                config.getLong(Core.getGame().getGameConfig().getActiveMap() + ".spawnLocation." + id + ".yaw"),
                config.getLong(Core.getGame().getGameConfig().getActiveMap() + ".spawnLocation." + id + ".pitch"));
    }

    private void stop() {
        Bukkit.broadcastMessage(Core.getGame().getGameConfig().getPrefix() + " §7Not enough players to start the game");
        Core.getGame().setGameState(GameState.WAITING);
        Bukkit.getServer().setWhitelist(false);
    }

}
