package core.skywars.lands.listeners;

import core.skywars.lands.Core;
import core.skywars.lands.data.PlayerData;
import core.skywars.lands.game.GameState;
import org.bukkit.GameMode;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;

public class ListenerPlayerDamage implements Listener {

    ArrayList<Player> coolDownVoid = new ArrayList<>();

    @EventHandler
    public void onPlayerDamage(EntityDamageEvent e) {
        if (Core.getGame().getGameState().equals(GameState.IN_GAME)) {
            if (e.getCause().equals(EntityDamageEvent.DamageCause.ENTITY_ATTACK) || e.getCause().equals(EntityDamageEvent.DamageCause.PROJECTILE)) {
                return;
            }

            if(e.getCause().equals(EntityDamageEvent.DamageCause.VOID)) {

                try {
                    Player player = (Player) e.getEntity();

                    if(Core.getGame().getPlayerData(player).getPlayer().getGameMode().equals(GameMode.SPECTATOR)) {
                        e.setCancelled(true);
                        return;
                    }

                    e.setCancelled(true);

                    if(!coolDownVoid.contains(player)) {
                        coolDownVoid.add(player);
                        PlayerData playerData = Core.getGame().getPlayerData(player);

                        player.setHealth(20);
                        if (playerData.getLastHit() != null) {
                            playerData.addDeath(playerData.getLastHit());
                        } else {
                            playerData.addDeath(null);
                        }

                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                coolDownVoid.remove(player);
                            }
                        }.runTaskLater(Core.getInstance(), 20);
                    }

                } catch (Exception exc) {

                }
                return;
            }

            try {
                Player player = (Player) e.getEntity();
                PlayerData playerData = Core.getGame().getPlayerData(player);

                if(playerData.isGrace()) {
                    e.setCancelled(true);
                    return;
                }

                if (player.getHealth() - e.getDamage() <= 0) {
                    e.setCancelled(true);
                    player.setHealth(20);
                    if (playerData.getLastHit() != null) {
                        playerData.addDeath(playerData.getLastHit());
                    } else {
                        playerData.addDeath(null);
                    }
                }

            } catch (Exception exc) {

            }
        } else {
            e.setCancelled(true);
            if(e.getCause().equals(EntityDamageEvent.DamageCause.VOID)) {
                Player player = (Player) e.getEntity();
                if(!coolDownVoid.contains(player)) {
                    Core.getGame().getPlayerData((Player) e.getEntity()).teleportLobby();
                    coolDownVoid.add(player);

                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            coolDownVoid.remove(player);
                        }
                    }.runTaskLater(Core.getInstance(), 20);
                }
            }
        }
    }

    @EventHandler
    public void onPlayerDamageByEntity(EntityDamageByEntityEvent e) {
        if (Core.getGame().getGameState().equals(GameState.IN_GAME)) {
            if (e.getCause().equals(EntityDamageEvent.DamageCause.ENTITY_ATTACK)) {
                try {
                    Player player = (Player) e.getEntity();
                    Player killer = (Player) e.getDamager();

                    PlayerData playerData = Core.getGame().getPlayerData(player);

                    if(playerData.isGrace()) {
                        e.setCancelled(true);
                        killer.sendMessage(Core.getGame().getGameConfig().getPrefix() + " §7This player is in the §dGrace Period§7.");
                        return;
                    }

                    if (player.getHealth() - e.getDamage() <= 0) {
                        e.setCancelled(true);
                        player.setHealth(20);
                        playerData.addDeath(killer);
                    } else {
                        playerData.setLastHit(killer);
                    }

                } catch (Exception exc) {

                }
            } else if (e.getCause().equals(EntityDamageEvent.DamageCause.PROJECTILE)) {
                try {
                    Player player = (Player) e.getEntity();
                    Arrow arrow = (Arrow) e.getDamager();

                    PlayerData playerData = Core.getGame().getPlayerData(player);

                    if(playerData.isGrace()) {
                        e.setCancelled(true);
                        ((Player)arrow.getShooter()).sendMessage(Core.getGame().getGameConfig().getPrefix() + " §7This player is in the §dGrace Period§7.");
                        return;
                    }

                    if (player.getHealth() - e.getDamage() <= 0) {
                        e.setCancelled(true);
                        player.setHealth(20);
                        playerData.addDeath((Player) arrow.getShooter());
                    } else {
                        playerData.setLastHit((Player) arrow.getShooter());
                    }

                } catch (Exception exc) {

                }
            }
        } else {
            e.setCancelled(true);
        }
    }

}
