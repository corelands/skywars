package core.skywars.lands.listeners;

import core.skywars.lands.Core;
import core.skywars.lands.data.PlayerData;
import core.skywars.lands.game.GameState;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ListenerPlayerChat implements Listener {

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {
        e.setCancelled(true);

        if (Core.getGame().getGameState().equals(GameState.IN_GAME)) {
            PlayerData playerData = Core.getGame().getPlayerData(e.getPlayer());
            Bukkit.broadcastMessage("§f" + e.getPlayer().getName() + "§7: " + e.getMessage());
        } else {
            Bukkit.broadcastMessage(Core.getGame().getGameConfig().getPrefix() + " §f" + e.getPlayer().getName() + "§7: " + e.getMessage());
        }
    }
}
