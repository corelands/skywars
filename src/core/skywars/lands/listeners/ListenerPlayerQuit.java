package core.skywars.lands.listeners;

import core.skywars.lands.Core;
import core.skywars.lands.data.PlayerData;
import core.skywars.lands.game.GameState;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class ListenerPlayerQuit implements Listener {

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        e.setQuitMessage(null);

        PlayerData playerData = Core.getGame().getPlayerData(e.getPlayer());

        Bukkit.broadcastMessage(Core.getGame().getGameConfig().getPrefix() + " §f" + playerData.getPlayer().getName() + " §7left the game §e(" + (Bukkit.getOnlinePlayers().size()-1) + "/" + Bukkit.getMaxPlayers() + ")");
        Core.getGame().removePlayerData(e.getPlayer());
        Core.getGame().getMySQL().setRoomPlayers(Bukkit.getOnlinePlayers().size()-1);
        Core.getGame().getMySQL().removeGlobalPlayer();

        if(Core.getGame().getGameState().equals(GameState.IN_GAME)) {
            if(Core.getGame().getGameConfig().getPlayersLeft() == 1) {
                for(Player player : Bukkit.getOnlinePlayers()) {
                    if(player.getGameMode().equals(GameMode.SPECTATOR)) {
                        continue;
                    }
                    PlayerData playerDataWinner = Core.getGame().getPlayerData(player);
                    playerDataWinner.win();
                }
            }
        }
    }

}
