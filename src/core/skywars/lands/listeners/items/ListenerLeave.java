package core.skywars.lands.listeners.items;

import core.skywars.lands.Core;
import core.skywars.lands.game.GameState;
import core.skywars.lands.managers.Hotbar;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class ListenerLeave implements Listener {

    @EventHandler
    public void onPlayerUseItem(PlayerInteractEvent e) {
        if(Core.getGame().getGameState().equals(GameState.WAITING) || Core.getGame().getGameState().equals(GameState.STARTING)) {
            if(e.getItem() != null && e.getItem().getType().equals(Hotbar.LEAVE.getItem().getType())) {
                ByteArrayOutputStream b = new ByteArrayOutputStream();
                DataOutputStream out = new DataOutputStream(b);
                try {
                    out.writeUTF("Connect");
                    out.writeUTF("lobby");
                } catch (IOException eee) {
                }
                e.getPlayer().sendPluginMessage(Core.getInstance(), "BungeeCord", b.toByteArray());
            }
        }
    }

}
