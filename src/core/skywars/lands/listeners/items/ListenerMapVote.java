package core.skywars.lands.listeners.items;

import core.skywars.lands.Core;
import core.skywars.lands.api.ItemCreator;
import core.skywars.lands.data.PlayerData;
import core.skywars.lands.game.GameState;
import core.skywars.lands.managers.Hotbar;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;

import java.util.Arrays;

public class ListenerMapVote implements Listener {

    private String name = "§eVote Map";

    private Inventory getInventory(Player player) {
        Inventory inventory = Bukkit.createInventory(player, 9 * 3, this.name);

        for(int i = 10; i < 10 + Core.getGame().getGameConfig().getMaps().size(); i++) {
            String map = Core.getGame().getGameConfig().getMaps().get(i-10);
            inventory.setItem(i, new ItemCreator(Material.GRASS_BLOCK).setName("§6" + map).setLore(Arrays.asList(" ", " §7Votes: §e" + Core.getGame().getGameConfig().getMapVotes(map), " ")).getItem());
        }

        return inventory;
    }

    @EventHandler
    public void onPlayerUseItem(PlayerInteractEvent e) {
        if(Core.getGame().getGameState().equals(GameState.WAITING) || Core.getGame().getGameState().equals(GameState.STARTING)) {
            if(e.getItem() != null && (e.getItem().getType().equals(Hotbar.VOTE.getItem().getType()))) {
                e.getPlayer().openInventory(getInventory(e.getPlayer()));
            }
        }
    }

    @EventHandler
    public void voteMap(InventoryClickEvent e) {
        if(e.getView().getTitle().equals(name)) {
            e.setCancelled(true);
            PlayerData playerData = Core.getGame().getPlayerData((Player) e.getWhoClicked());
            int id = e.getSlot()-10;
            if(!(id >= 0 && id < Core.getGame().getGameConfig().getMaps().size())) {
                return;
            }
            String map = Core.getGame().getGameConfig().getMaps().get(e.getSlot()-10);
            if(playerData.getMapVote() != null) {
                Core.getGame().getGameConfig().removeMapVote(playerData.getMapVote());
            }
            playerData.setMapVote(map);
            Core.getGame().getGameConfig().addMapVote(map);

            e.getWhoClicked().sendMessage(Core.getGame().getGameConfig().getPrefix() + " §7You voted on the map §e" + map + "§7.");
            e.getWhoClicked().closeInventory();
            e.getWhoClicked().openInventory(getInventory((Player) e.getWhoClicked()));
        }
    }

}
