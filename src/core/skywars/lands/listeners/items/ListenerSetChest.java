package core.skywars.lands.listeners.items;

import core.skywars.lands.Core;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.material.Directional;

public class ListenerSetChest implements Listener {

    private static int id = 0;

    @EventHandler
    public void onPlayerSetChest(PlayerInteractEvent e) {
        if(e.getPlayer().isOp()) {
            try {
                Block block = e.getClickedBlock();

                FileConfiguration config = Core.getInstance().getConfig();

                String type = e.getPlayer().getItemInHand().getItemMeta().getDisplayName();

                config.set(block.getWorld().getName() + ".chest." + type + "." + id + ".world", block.getWorld().getName());
                config.set(block.getWorld().getName() + ".chest." + type + "." + id + ".x", block.getX());
                config.set(block.getWorld().getName() + ".chest." + type + "." + id + ".y", block.getY());
                config.set(block.getWorld().getName() + ".chest." + type + "." + id + ".z", block.getZ());
                config.set(block.getWorld().getName() + ".chest." + type + "." + id + ".face", ((Directional) block.getState().getData()).getFacing().toString());

                block.setType(Material.CHEST);

                Core.getInstance().saveConfig();
                Core.getInstance().reloadConfig();

                e.getPlayer().sendMessage(Core.getGame().getGameConfig().getPrefix() + " §aYou have one chest §d" + type + "§a for the map §e" + block.getWorld().getName());
                id++;
            } catch (Exception exc) {
                exc.printStackTrace();
            }
        }
    }

}
