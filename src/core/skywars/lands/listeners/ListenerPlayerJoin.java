package core.skywars.lands.listeners;

import core.skywars.lands.Core;
import core.skywars.lands.data.PlayerData;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class ListenerPlayerJoin implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        e.setJoinMessage(null);

        PlayerData playerData = Core.getGame().addPlayerData(e.getPlayer());

        Bukkit.broadcastMessage(Core.getGame().getGameConfig().getPrefix() + " §f" + playerData.getPlayer().getName() + " §7joined the game §e(" + Bukkit.getOnlinePlayers().size() + "/" + Bukkit.getMaxPlayers() + ")");
        Core.getGame().getMySQL().setRoomPlayers(Bukkit.getOnlinePlayers().size());
        Core.getGame().getMySQL().addGlobalPlayer();
    }

}
