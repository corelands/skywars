package core.skywars.lands.listeners;

import core.skywars.lands.Core;
import core.skywars.lands.game.GameState;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

public class ListenerMainProtection implements Listener {

    @EventHandler
    public void onPlace(BlockPlaceEvent e) {
        e.setCancelled(!Core.getGame().getGameState().equals(GameState.IN_GAME));
    }

    @EventHandler
    public void onBreak(BlockBreakEvent e) {

    }

    @EventHandler
    public void onPickupItem(PlayerPickupItemEvent e) {
        e.setCancelled(!Core.getGame().getGameState().equals(GameState.IN_GAME));
    }

    @EventHandler
    public void onDropItem(PlayerDropItemEvent e) {
        e.setCancelled(!Core.getGame().getGameState().equals(GameState.IN_GAME));
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        e.setCancelled(!Core.getGame().getGameState().equals(GameState.IN_GAME));
    }

    @EventHandler
    public void onWeatherChange(WeatherChangeEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onSpawnMobs(CreatureSpawnEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onFood(FoodLevelChangeEvent e) {
        e.setCancelled(!Core.getGame().getGameState().equals(GameState.IN_GAME));
    }

}
