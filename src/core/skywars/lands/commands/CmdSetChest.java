package core.skywars.lands.commands;

import core.skywars.lands.api.ItemCreator;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdSetChest implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if(commandSender.isOp()) {
            Player player = (Player) commandSender;

            player.getInventory().setItem(0, new ItemCreator(Material.STICK).setName(args[0]).getItem());

        }
        return false;
    }
}
