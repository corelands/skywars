package core.skywars.lands.commands;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdWorld implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if(commandSender.isOp()) {
            try {
                Player player = (Player) commandSender;

                Location loc = new Location(Bukkit.getWorld(args[0]), 0, 0, 0);

                player.teleport(loc);
            } catch (Exception exc) {

            }
        }
        return false;
    }
}
