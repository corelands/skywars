package core.skywars.lands.commands;

import core.skywars.lands.Core;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class CmdSetLocation implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if(commandSender.isOp()) {
            Player player = (Player) commandSender;

            if(args.length == 1) {
                String location = args[0].toLowerCase();

                FileConfiguration config = Core.getInstance().getConfig();

                config.set(".location." + location + ".world", player.getWorld().getName());
                config.set(".location." + location + ".x", player.getLocation().getX());
                config.set(".location." + location + ".y", player.getLocation().getY());
                config.set(".location." + location + ".z", player.getLocation().getZ());
                config.set(".location." + location + ".yaw", player.getLocation().getYaw());
                config.set(".location." + location + ".pitch", player.getLocation().getPitch());

                Core.getInstance().saveConfig();
                Core.getInstance().reloadConfig();

                player.sendMessage(Core.getGame().getGameConfig().getPrefix() + " §aYou have set the location §e" + args[0]);
            } else {
                int id = Integer.parseInt(args[0]);
                String map = args[1];

                FileConfiguration config = Core.getInstance().getConfig();

                config.set(map + ".spawnLocation." + id + ".world", player.getWorld().getName());
                config.set(map + ".spawnLocation." + id + ".x", player.getLocation().getX());
                config.set(map + ".spawnLocation." + id + ".y", player.getLocation().getY());
                config.set(map + ".spawnLocation." + id + ".z", player.getLocation().getZ());
                config.set(map + ".spawnLocation." + id + ".yaw", player.getLocation().getYaw());
                config.set(map + ".spawnLocation." + id + ".pitch", player.getLocation().getPitch());

                Core.getInstance().saveConfig();
                Core.getInstance().reloadConfig();

                player.sendMessage(Core.getGame().getGameConfig().getPrefix() + " §aYou have set the location §e" + args[0] + "§a for the map §d" + args[1] + "§a.");
            }
        }
        return false;
    }
}