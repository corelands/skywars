package core.skywars.lands.game;

import core.skywars.lands.Core;

import java.util.ArrayList;
import java.util.HashMap;

public class GameConfig {

    private String prefix = "§7[§bSkyWars§7]", activeMap;
    private ArrayList<String> maps;
    private HashMap<String, Integer> mapVoting;
    private int playersLeft;

    public GameConfig() {
        maps = new ArrayList<>();
        mapVoting = new HashMap<>();
        activeMap = "";
    }

    public String getPrefix() {
        return this.prefix;
    }

    public String getActiveMap() {
        return this.activeMap;
    }

    public int getStartTime() {
        return 61;
    }

    public int getMinPlayers() {
        return 2;
    }

    public void addMap(String map) {
        this.maps.add(map);
        this.mapVoting.put(map, 0);
    }

    public int getMapVotes(String map) {
        return this.mapVoting.get(map);
    }

    public void addMapVote(String map) {
        this.mapVoting.put(map, getMapVotes(map) + 1);
    }

    public void removeMapVote(String map) {
        this.mapVoting.put(map, getMapVotes(map) - 1);
    }

    public ArrayList<String> getMaps() {
        return this.maps;
    }

    public String getRoomID() {
        return Core.getInstance().getConfig().getString("room");
    }

    public void setActiveMap(String map) {
        this.activeMap = map;
    }

    public void setPlayersLeft(int playersLeft) {
        this.playersLeft = playersLeft;
    }

    public int getPlayersLeft() {
        return this.playersLeft;
    }

}
