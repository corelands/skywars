package core.skywars.lands.game.chests;

import core.skywars.lands.api.ItemCreator;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionType;

import java.util.ArrayList;

public class ChestTypes {

    public ArrayList<ItemStack> getIslandChests() {
        ArrayList<ItemStack> islandChest = new ArrayList<>();

        islandChest.add(new ItemCreator(Material.OAK_LOG).setAmount(5).getItem());
        islandChest.add(new ItemCreator(Material.STONE).setAmount(16).getItem());
        islandChest.add(new ItemCreator(Material.EXPERIENCE_BOTTLE).setAmount(4).getItem());
        islandChest.add(new ItemCreator(Material.STONE_SWORD).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.ENCHANTING_TABLE).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.BOW).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.ARROW).setAmount(3).getItem());
        islandChest.add(new ItemCreator(Material.CHAINMAIL_HELMET).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.WOODEN_SWORD).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.LEATHER_LEGGINGS).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.CHAINMAIL_CHESTPLATE).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.LEATHER_BOOTS).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.SPLASH_POTION).addEffect(PotionType.INSTANT_DAMAGE, 1, false, false).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.IRON_INGOT).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.DIAMOND).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.DAMAGED_ANVIL).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.COOKED_BEEF).setAmount(6).getItem());
        islandChest.add(new ItemCreator(Material.LEATHER).setAmount(8).getItem());
        islandChest.add(new ItemCreator(Material.ENDER_PEARL).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.COOKED_BEEF).setAmount(3).getItem());
        islandChest.add(new ItemCreator(Material.GOLDEN_APPLE).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.STICK).setAmount(2).getItem());
        islandChest.add(new ItemCreator(Material.COBWEB).setAmount(3).getItem());
        islandChest.add(new ItemCreator(Material.WOODEN_AXE).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.TURTLE_HELMET).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.FLINT_AND_STEEL).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.TNT).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.STONE).setAmount(8).getItem());


        return islandChest;
    }

    public ArrayList<ItemStack> getSemiMidChests() {
        ArrayList<ItemStack> islandChest = new ArrayList<>();

        islandChest.add(new ItemCreator(Material.OAK_WOOD).setAmount(16).getItem());
        islandChest.add(new ItemCreator(Material.ARROW).setAmount(8).getItem());
        islandChest.add(new ItemCreator(Material.CRAFTING_TABLE).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.IRON_SWORD).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.IRON_HELMET).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.IRON_CHESTPLATE).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.IRON_LEGGINGS).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.IRON_BOOTS).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.ENCHANTING_TABLE).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.DIAMOND).setAmount(2).getItem());
        islandChest.add(new ItemCreator(Material.IRON_INGOT).setAmount(6).getItem());
        islandChest.add(new ItemCreator(Material.DAMAGED_ANVIL).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.GOLDEN_APPLE).setAmount(3).getItem());
        islandChest.add(new ItemCreator(Material.COOKED_BEEF).setAmount(10).getItem());
        islandChest.add(new ItemCreator(Material.BOW).addEnchantment(Enchantment.ARROW_DAMAGE, 1).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.EXPERIENCE_BOTTLE).setAmount(16).getItem());
        islandChest.add(new ItemCreator(Material.COBWEB).setAmount(4).getItem());
        islandChest.add(new ItemCreator(Material.STONE_BRICKS).setAmount(16).getItem());
        islandChest.add(new ItemCreator(Material.TURTLE_HELMET).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.STONE).setAmount(8).getItem());
        islandChest.add(new ItemCreator(Material.STICK).setAmount(2).getItem());
        islandChest.add(new ItemCreator(Material.STONE_BRICKS).setAmount(16).getItem());
        islandChest.add(new ItemCreator(Material.IRON_AXE).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.STONE_BRICKS).setAmount(5).getItem());
        islandChest.add(new ItemCreator(Material.WATER_BUCKET).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.STONE_BRICKS).setAmount(10).getItem());
        islandChest.add(new ItemCreator(Material.CROSSBOW).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.ENDER_PEARL).setAmount(2).getItem());
        islandChest.add(new ItemCreator(Material.SPLASH_POTION).addEffect(PotionType.INSTANT_DAMAGE, 1, false, false).setAmount(1).getItem());

        return islandChest;
    }

    public ArrayList<ItemStack> getMidChests() {
        ArrayList<ItemStack> islandChest = new ArrayList<>();

        islandChest.add(new ItemCreator(Material.OAK_WOOD).setAmount(32).getItem());
        islandChest.add(new ItemCreator(Material.DIAMOND).setAmount(6).getItem());
        islandChest.add(new ItemCreator(Material.WATER_BUCKET).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.COOKED_BEEF).setAmount(30).getItem());
        islandChest.add(new ItemCreator(Material.STONE_BRICKS).setAmount(16).getItem());
        islandChest.add(new ItemCreator(Material.LAVA_BUCKET).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.IRON_CHESTPLATE).addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.FLINT_AND_STEEL).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.STONE).setAmount(32).getItem());
        islandChest.add(new ItemCreator(Material.SPLASH_POTION).addEffect(PotionType.INSTANT_HEAL, 1, false, false).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.SPLASH_POTION).addEffect(PotionType.STRENGTH, 1, false, false).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.ENCHANTING_TABLE).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.ARROW).setAmount(16).getItem());
        islandChest.add(new ItemCreator(Material.DAMAGED_ANVIL).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.BOW).addEnchantment(Enchantment.ARROW_KNOCKBACK, 1).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.TNT).setAmount(3).getItem());
        islandChest.add(new ItemCreator(Material.EXPERIENCE_BOTTLE).setAmount(32).getItem());
        islandChest.add(new ItemCreator(Material.COBWEB).setAmount(4).getItem());
        islandChest.add(new ItemCreator(Material.DIAMOND_HELMET).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.DIAMOND_CHESTPLATE).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.DIAMOND_CHESTPLATE).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.DIAMOND_BOOTS).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.DIAMOND_SWORD).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.GOLDEN_APPLE).setAmount(5).getItem());
        islandChest.add(new ItemCreator(Material.IRON_HELMET).addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.STICK).setAmount(3).getItem());
        islandChest.add(new ItemCreator(Material.CROSSBOW).setAmount(1).getItem());
        islandChest.add(new ItemCreator(Material.ENDER_PEARL).setAmount(3).getItem());
        islandChest.add(new ItemCreator(Material.SPLASH_POTION).addEffect(PotionType.INSTANT_DAMAGE, 1, false, false).setAmount(1).getItem());

        return islandChest;
    }

}
