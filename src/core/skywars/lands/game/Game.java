package core.skywars.lands.game;

import core.skywars.lands.api.MySQL;
import core.skywars.lands.data.PlayerData;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class Game {

    private GameConfig gameConfig;
    private HashMap<Player, PlayerData> cache;
    private MySQL mySQL;
    private GameState gameState;

    public Game() {
        this.gameConfig = new GameConfig();
        this.cache = new HashMap<>();
        this.gameState = GameState.WAITING;
        this.mySQL = new MySQL("localhost", "corelands", "debian-sys-maint", "WnCqLjDzdtGlr1CL", 3306);
        this.mySQL.connect();
    }

    public GameState getGameState() {
        return this.gameState;
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }

    public GameConfig getGameConfig() {
        return this.gameConfig;
    }

    public PlayerData addPlayerData(Player player) {
        this.cache.put(player, new PlayerData(player));
        return this.cache.get(player);
    }

    public PlayerData getPlayerData(Player player) {
        return this.cache.get(player);
    }

    public void removePlayerData(Player player) {
        if(getPlayerData(player).getMapVote() != null) {
            getGameConfig().removeMapVote(getPlayerData(player).getMapVote());
        }
        this.cache.remove(player);
    }

    public MySQL getMySQL() {
        return this.mySQL;
    }

}
