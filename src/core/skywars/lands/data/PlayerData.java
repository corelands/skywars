package core.skywars.lands.data;

import core.skywars.lands.Core;
import core.skywars.lands.api.InstantFirework;
import core.skywars.lands.api.ParticleEffect;
import core.skywars.lands.game.GameState;
import core.skywars.lands.managers.Hotbar;
import core.skywars.lands.managers.scoreboard.SkyWarsScoreboardManager;
import core.skywars.lands.runnables.StartTimer;
import org.bukkit.*;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class PlayerData {

    private Player player, lastHit;
    private boolean gracePeriod;
    private String mapVote;
    private Location island;

    public PlayerData(Player player) {
        this.player = player;

        player.setGameMode(GameMode.ADVENTURE);
        teleportLobby();
        giveHotbar();

        player.setItemOnCursor(null);
        player.setLevel(0);
        player.setExp(0);
        player.setHealth(20D);
        player.setFoodLevel(20);

        new SkyWarsScoreboardManager(player).setScoreboard();

        for(PotionEffect potionEffect : player.getActivePotionEffects()) {
            player.removePotionEffect(potionEffect.getType());
        }

        this.tryStartGame();
        gracePeriod = true;
    }

    public Player getPlayer() {
        return this.player;
    }

    public void addDeath(Player killer) {
        this.lastHit = null;
        Core.getGame().getGameConfig().setPlayersLeft(Core.getGame().getGameConfig().getPlayersLeft()-1);

        for(Player all : Bukkit.getOnlinePlayers()) {
            if(all != player) {
                all.hidePlayer(Core.getInstance(), player);
            }
        }

        for(int i = 0; i < 15; i++) {
            ParticleEffect.FIREWORKS_SPARK.display(player, player.getLocation().clone().add(Math.random(), Math.random(), Math.random()), 10, 2);
        }

        if(killer == null) {
            Bukkit.broadcastMessage(Core.getGame().getGameConfig().getPrefix() + " §f" + this.player.getName() + " §7died.");
        } else {
            Core.getGame().getPlayerData(killer).addKill();
            Bukkit.broadcastMessage(Core.getGame().getGameConfig().getPrefix() + " §f" + this.player.getName() + " §7has been killed by §f" + killer.getName() + "§7.");
        }

        this.dropItems();

        player.getInventory().clear();
        player.getInventory().setArmorContents(null);
        player.getInventory().setItemInMainHand(null);
        player.getInventory().setItemInOffHand(null);
        player.setItemOnCursor(null);
        player.setGameMode(GameMode.SPECTATOR);

        this.teleportIsland();

        player.playSound(player.getLocation(), Sound.ENTITY_LIGHTNING_BOLT_THUNDER, 1f, 1f);

        if(Core.getGame().getGameState().equals(GameState.IN_GAME)) {
            if(Core.getGame().getGameConfig().getPlayersLeft() == 1) {
                for(Player player : Bukkit.getOnlinePlayers()) {
                    if(player.getGameMode().equals(GameMode.SPECTATOR)) {
                        continue;
                    }
                    PlayerData playerDataWinner = Core.getGame().getPlayerData(player);
                    playerDataWinner.win();
                }
            }
        }
    }

    public void addKill() {
        this.player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1f, 1f);
    }

    private void giveKit() {
        player.getInventory().clear();
    }

    public void spawn() {
        gracePeriod = true;

        new BukkitRunnable() {
            @Override
            public void run() {
                gracePeriod = false;
                player.sendMessage(Core.getGame().getGameConfig().getPrefix() + " §7Spawn protection removed.");
            }
        }.runTaskLater(Core.getInstance(), 20*5);

        this.player.setGameMode(GameMode.SURVIVAL);
        this.player.setHealth(20D);
        this.player.setFoodLevel(20);

        giveKit();
    }

    public void setLastHit(Player lastHit) {
        this.lastHit = lastHit;
    }

    public Player getLastHit() {
        return this.lastHit;
    }

    public boolean isGrace() {
        return this.gracePeriod;
    }

    public void setMapVote(String mapVote) {
        this.mapVote = mapVote;
    }

    public String getMapVote() {
        return this.mapVote;
    }

    public void teleportLobby() {
        this.player.teleport(getLobby());
    }

    public void setIsland(Location island) {
        this.island = island;
    }

    public void teleportIsland() {
        player.teleport(this.island);
    }

    public Location getIsland() {
        return this.island;
    }

    private void giveHotbar() {
        player.getInventory().clear();;
        player.getInventory().setArmorContents(null);
        player.setItemOnCursor(null);
        for(Hotbar hotbar : Hotbar.values()) {
            player.getInventory().setItem(hotbar.getSlot(), hotbar.getItem());
        }
    }

    public void win() {
        Core.getGame().setGameState(GameState.RESTARTING);

        Bukkit.broadcastMessage(Core.getGame().getGameConfig().getPrefix() + " §f" + player.getName() + " §7won the game.");

        for(Player all : Bukkit.getOnlinePlayers()) {
            all.sendTitle("§f" + player.getName() + " §eWINS", "", 20, 40, 20);
            all.setAllowFlight(true);
            all.setFlying(true);
        }

        new BukkitRunnable() {
            int time = 0;
            @Override
            public void run() {
                time++;
                for(Player all : Bukkit.getOnlinePlayers()) {
                    new InstantFirework(FireworkEffect.builder().flicker(false).trail(true).with(FireworkEffect.Type.BALL).withColor(Color.BLUE).withFade(Color.WHITE).build(), all.getLocation());
                }
                if(time == 5) {

                    ByteArrayOutputStream b = new ByteArrayOutputStream();
                    DataOutputStream out = new DataOutputStream(b);
                    try {
                        out.writeUTF("Connect");
                        out.writeUTF("lobby");
                    } catch (IOException eee) {
                    }
                    for(Player all : Bukkit.getOnlinePlayers()) {
                        all.sendPluginMessage(Core.getInstance(), "BungeeCord", b.toByteArray());
                    }
                }
                if(time == 7) {
                    Bukkit.getServer().shutdown();
                }
            }
        }.runTaskTimer(Core.getInstance(), 0, 20);
    }

    private Location getLobby() {
        FileConfiguration config = Core.getInstance().getConfig();
        return new Location(Bukkit.getWorld(config.getString("location.lobby.world")), config.getDouble("location.lobby.x"), config.getDouble("location.lobby.y"), config.getDouble("location.lobby.z"),config.getLong("location.lobby.yaw"), config.getLong("location.lobby.pitch"));
    }

    private void tryStartGame() {
        if(Bukkit.getOnlinePlayers().size() >= Core.getGame().getGameConfig().getMinPlayers() && Core.getGame().getGameState().equals(GameState.WAITING)) {
            Core.getGame().setGameState(GameState.STARTING);
            new StartTimer().runTaskTimer(Core.getInstance(), 0, 20);
        }
    }

    private void dropItems() {
        try {
            player.getWorld().dropItem(player.getLocation(), player.getItemOnCursor());
        } catch(Exception exc) {

        }
        for(ItemStack itemStack : player.getInventory()) {
            if(itemStack != null) {
                player.getWorld().dropItem(player.getLocation(), itemStack);
            }
        }
    }

}
