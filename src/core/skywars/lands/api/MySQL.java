package core.skywars.lands.api;

import core.skywars.lands.Core;
import core.skywars.lands.game.GameState;
import org.bukkit.Bukkit;

import java.sql.*;

public class MySQL {

    private Connection connection;

    private String host, database, username, password;
    private int port;

    public MySQL(String host, String database, String username, String password, int port) {
        this.host = host;
        this.database = database;
        this.username = username;
        this.password = password;
        this.port = port;
    }

    public void connect() {
        try {
            synchronized (this) {
                if(getConnection() != null && !getConnection().isClosed()) {
                    return;
                }

                Class.forName("com.mysql.jdbc.Driver");
                setConnection( DriverManager.getConnection("jdbc:mysql://" + this.host + ":" + this.port + "/" + this.database + "/confluence?autoReconnect=true", this.username, this.password));
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private boolean roomExists(String room) {
        try {
            PreparedStatement preparedStatement = getConnection().prepareStatement("SELECT * FROM skywars_rooms WHERE ROOM_ID=?");
            preparedStatement.setString(1, room);

            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()) {
                return true;
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean existsGlobalRoom(String room) {
        try {
            PreparedStatement preparedStatement = getConnection().prepareStatement("SELECT * FROM skywars_rooms WHERE ROOM_ID=?");
            preparedStatement.setString(1, room);

            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()) {
                return true;
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void createRoom() {
        try {
            if(!existsGlobalRoom("skywars")) {
                PreparedStatement preparedStatement = getConnection().prepareStatement("INSERT INTO skywars_rooms (ROOM_ID, STATUS, ONLINE_PLAYERS, MAX_PLAYERS) VALUES (?, ?, ?, ?)");
                preparedStatement.setString(1, "skywars");
                preparedStatement.setString(2, GameState.WAITING.toString());
                preparedStatement.setInt(3, 0);
                preparedStatement.setInt(4, 0);

                preparedStatement.executeUpdate();
            }
            if(!roomExists(Core.getGame().getGameConfig().getRoomID())) {
                PreparedStatement preparedStatement = getConnection().prepareStatement("INSERT INTO skywars_rooms (ROOM_ID, STATUS, ONLINE_PLAYERS, MAX_PLAYERS) VALUES (?, ?, ?, ?)");
                preparedStatement.setString(1, Core.getGame().getGameConfig().getRoomID());
                preparedStatement.setString(2, GameState.WAITING.toString());
                preparedStatement.setInt(3, 0);
                preparedStatement.setInt(4, Bukkit.getMaxPlayers());

                preparedStatement.executeUpdate();
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }

    public void setRoomState(String state) {
        try {
            PreparedStatement preparedStatement = getConnection().prepareStatement("UPDATE skywars_rooms SET STATUS=? WHERE ROOM_ID=?");
            preparedStatement.setString(1, state);
            preparedStatement.setString(2, Core.getGame().getGameConfig().getRoomID());
            preparedStatement.executeUpdate();
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }

    public void setRoomPlayers(int onlinePlayers) {
        try {
            PreparedStatement preparedStatement = getConnection().prepareStatement("UPDATE skywars_rooms SET ONLINE_PLAYERS=? WHERE ROOM_ID=?");
            preparedStatement.setInt(1, onlinePlayers);
            preparedStatement.setString(2, Core.getGame().getGameConfig().getRoomID());
            preparedStatement.executeUpdate();

        } catch(SQLException e) {
            e.printStackTrace();
        }
    }

    public void addGlobalPlayer() {
        try {
            PreparedStatement preparedStatement = getConnection().prepareStatement("UPDATE skywars_rooms SET ONLINE_PLAYERS=? WHERE ROOM_ID=?");
            preparedStatement.setInt(1, getGlobalPlayers()+1);
            preparedStatement.setString(2, "skywars");
            preparedStatement.executeUpdate();

        } catch(SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeGlobalPlayer() {
        try {
            PreparedStatement preparedStatement = getConnection().prepareStatement("UPDATE skywars_rooms SET ONLINE_PLAYERS=? WHERE ROOM_ID=?");
            preparedStatement.setInt(1, getGlobalPlayers()-1);
            preparedStatement.setString(2, "skywars");
            preparedStatement.executeUpdate();

        } catch(SQLException e) {
            e.printStackTrace();
        }
    }

    public int getGlobalPlayers() {
        try {
            PreparedStatement preparedStatement = getConnection().prepareStatement("SELECT * FROM skywars_rooms WHERE ROOM_ID=?");
            preparedStatement.setString(1, "skywars");
            ResultSet results = preparedStatement.executeQuery();
            results.next();

            return results.getInt("ONLINE_PLAYERS");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    private Connection getConnection() {
        return this.connection;
    }

    private void setConnection(Connection connection) {
        this.connection = connection;
    }

}
