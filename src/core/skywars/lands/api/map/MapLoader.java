package core.skywars.lands.api.map;

import core.skywars.lands.Core;
import org.bukkit.Bukkit;
import org.bukkit.GameRule;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.craftbukkit.libs.org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

public class MapLoader {

    private String minigame;
    private String directories[];

    public MapLoader(String minigame) {
        this.minigame = minigame;
    }

    public MapLoader setupMaps() {
        World lobby = new WorldCreator(Core.getInstance().getConfig().getString("location.lobby.world")).environment(World.Environment.NORMAL).createWorld();
        lobby.setAutoSave(false);
        lobby.setGameRule(GameRule.DO_DAYLIGHT_CYCLE, false);
        lobby.setGameRule(GameRule.ANNOUNCE_ADVANCEMENTS, false);
        lobby.setTime(6000);
        File file = new File("../../maps/" + minigame);
        String[] directories = file.list(new FilenameFilter() {
            @Override
            public boolean accept(File current, String name) {
                return new File(current, name).isDirectory();
            }
        });

        this.directories = directories;

        for(String map : directories) {
            Core.getGame().getGameConfig().addMap(map);
            loadMap(map);
        }

        return this;
    }

    private void loadMap(String map) {
        Bukkit.getConsoleSender().sendMessage(Core.getGame().getGameConfig().getPrefix() + " §7Loading map §e" + map);
        try {
            FileUtils.copyDirectory(new File("../../maps/" + minigame + "/" + map), new File(map));
        } catch(IOException exc) {

        }
        World world = new WorldCreator(map).environment(World.Environment.NORMAL).createWorld();
        world.setAutoSave(false);
        world.setGameRule(GameRule.DO_DAYLIGHT_CYCLE, false);
        world.setGameRule(GameRule.ANNOUNCE_ADVANCEMENTS, false);
        world.setTime(6000);
        Bukkit.getConsoleSender().sendMessage(Core.getGame().getGameConfig().getPrefix() + " §7Map loaded.");
    }

    public void removeMaps() {
        for(String map : directories) {
            unloadMap(map);
        }
    }

    private void unloadMap(String map) {
        Bukkit.getConsoleSender().sendMessage(Core.getGame().getGameConfig().getPrefix() + " §7Unloading map §e" + map);
        try {
            FileUtils.deleteDirectory(new File(map));
            Bukkit.unloadWorld(Bukkit.getWorld(map), false);
        } catch(Exception exc) {

        }
        Bukkit.getConsoleSender().sendMessage(Core.getGame().getGameConfig().getPrefix() + " §7Map unloaded.");
    }

}
